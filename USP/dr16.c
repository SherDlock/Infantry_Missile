#include "dr16.h"
#include "tim.h"

extern float GM3508_target_speed; 

extern float PITCH_GM6020_target_speed ;
extern float PITCH_GM6020_target_angle_sys;  //(引入角度制）

extern float YAW_GM6020_target_speed;  //这个是对gyroz进行控制
extern float YAW_GM6020_target_angle;

extern float GM2006_target_angle;   //初值置0(引入角度制）

extern float Remote_magazine_value;

uint8_t Remote_flag=0;        //遥控标志位
uint8_t dial_flag= 2;          //拨盘标志位
uint8_t magazine_flag = 2;    //弹仓标志位

RC_Ctl_t RC_CtrlData; //用于接收解包后的数据


/*DBUS数据解包函数，用一个Remote_Control数据结构体储存*/
void RemoteDataProcess(uint8_t *pData)
{
  if(pData == NULL)
	{
	  return;
	}
	
	RC_CtrlData.rc.ch0 = ((int16_t)pData[0] | ((int16_t)pData[1] << 8)) & 0x07FF;
  RC_CtrlData.rc.ch1 = (((int16_t)pData[1] >> 3) | ((int16_t)pData[2] << 5)) & 0x07FF;
  RC_CtrlData.rc.ch2 = (((int16_t)pData[2] >> 6) | ((int16_t)pData[3] << 2) | ((int16_t)pData[4] << 10)) & 0x07FF;
  RC_CtrlData.rc.ch3 = (((int16_t)pData[4] >> 1) | ((int16_t)pData[5]<<7)) & 0x07FF;
 
  RC_CtrlData.rc.s1 = ((pData[5] >> 4) & 0x000C) >> 2;
  RC_CtrlData.rc.s2 = ((pData[5] >> 4) & 0x0003);
	
  RC_CtrlData.mouse.x = ((int16_t)pData[6]) | ((int16_t)pData[7] << 8);
  RC_CtrlData.mouse.y = ((int16_t)pData[8]) | ((int16_t)pData[9] << 8);
  RC_CtrlData.mouse.z = ((int16_t)pData[10]) | ((int16_t)pData[11] << 8); 
  RC_CtrlData.mouse.press_l = pData[12];
  RC_CtrlData.mouse.press_r = pData[13];
	
  RC_CtrlData.key.v = ((int16_t)pData[14]);// | ((int16_t)pData[15] << 8);
	
	Remote_flag = 0;    //消息接收完毕，标志位置0
}

/*
	遥控摩擦轮函数
  可手动关闭摩擦轮
*/
void Remote_friction(void)
{
	if     (RC_CtrlData.rc.s1==2)   GM3508_target_speed=0;          //停止发射
	else if(RC_CtrlData.rc.s1==1)   GM3508_target_speed=3500;       //进入强力发射模式
	else                            GM3508_target_speed=2000;       //进入普通发射模式
}

/*
	遥控拨弹盘函数
	s2拨到3关闭拨弹模式
*/
void Remote_dial(void)
{
		if(RC_CtrlData.rc.s2==2)     //s2值为2时，可通过左摇杆手动移拨动拨盘
	    dial_flag = 1;
	  if(dial_flag==1)
		{
//			if(RC_CtrlData.rc.s2==3)  
//			{
//	      dial_flag = 0;
//		    GM2006_target_angle -=1296;
//			}
			GM2006_target_angle += (RC_CtrlData.rc.ch2-1024)*0.001*8;       //参数自己调整,手动调整PID参数
		}	
	
	
//	if     (RC_CtrlData.rc.s2==2)   GM2006_target_angle += (RC_CtrlData.rc.ch2-1024)*0.01*4;           //可以手动调整发射速度
//	if     (RC_CtrlData.rc.s2==2)   GM2006_target_angle += (1684              -1024)*0.001*4;          //低频发射
//	else if(RC_CtrlData.rc.s2==1)   GM2006_target_angle += (1684              -1024)*0.001*8;          //快速发射（2倍速）
		
}

/*
	遥控Pitch轴函数
	调节参数使遥控能均匀控制pitch轴
*/
void Remote_pitch(void)
{
	//参数待调
	PITCH_GM6020_target_angle_sys -= ((RC_CtrlData.rc.ch1-1024)*0.001*6.5)/8191.0f*360.0f;
//	PITCH_GM6020_target_angle -= (RC_CtrlData.rc.ch1-1024)*0.001*6.5;
}

/*
	遥控Yaw轴函数
	调节参数使遥控能均匀控制yaw轴
*/
void Remote_yaw(void)
{
	//参数待调
	YAW_GM6020_target_angle -= (RC_CtrlData.rc.ch0-1024)*0.001*0.8;
}


















/*
	遥控弹仓盖函数
	调节参数使遥控能均匀控制弹仓盖舵机
*/
void Remote_Magazine_Servo(void)
{
  if(RC_CtrlData.rc.s2==1)
	  magazine_flag = 1;
	if(magazine_flag==1&&RC_CtrlData.rc.s2==3)
	{
	  magazine_flag= 0;
		Remote_magazine_value +=300;
		__HAL_TIM_SetCompare(&htim5,TIM_CHANNEL_2,Remote_magazine_value);		
	}	
}
