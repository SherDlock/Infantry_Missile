#ifndef __PID_H
#define __PID_H

#include "main.h"

#define Limit(x,min,max) (x) = (((x)<=(min))?(min):(((x)>=(max))?(max):(x)))  //积分限幅
#define NOW 0
#define LAST 1
#define LAST_LAST 2
#define Position 0       //位置模式
#define Change 1        //增量模式

//定义PID数据结构体类型
typedef struct
{
  float kp;    
	float ki;
	float kd;
	
	float target_value;    //期望值
	float feedback_value;  //实际值
	float errors[3];       //偏差
	float max_errors;      //最大偏差
	float p_out;
	float i_out;
	float d_out;
	float out_value;
	
	uint8_t mode; //输入0 位置PID，1增量PID
/*
	位置型控制算法：
缺点：每次输出均与过去的状态有关，计算时要对e(k)进行累加，计算机运算工作量大。
  
	增量型控制算法：
优点：
①误动作时影响小，必要时可用逻辑判断的方法去掉出错数据。
②手动/自动切换时冲击小，便于实现无扰动切换。当计算机故障时，仍能保持原值。
③算式中不需要累加。
缺点：积分截断效应大，有稳态误差；溢出的影响大。
*/
	
	int32_t Max_Output;				//输出限幅
  int32_t Integral_Limit;		//积分限幅

}pid_t;

void pid_init(pid_t *,uint8_t ,float ,float ,float ,uint32_t ,uint32_t );
void pid_calcu(pid_t *,float ,float );

#endif
