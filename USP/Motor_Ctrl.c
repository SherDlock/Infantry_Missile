#include "Motor_Ctrl.h"
#include "PID.h"
#include "dr16.h"

/***********
						驱动部位						电机            ID         motor_info序号
CAN1：  		   拨弹盘           GM2006             1              0
						 摩擦轮1		      GM3508             2              1
						 摩擦轮2         GM3508             3              2
						 Pitch轴          GM6020             2              4

CAN2：        Yaw轴            GM6020             1              3

id_range：1、2、3、4——>0     
					5、6、7、8——>1		 
					
GM3508数据范围：-16384~16384
GM2006数据范围：-10000~10000
GM6020数据范围：-30000~30000
************/
motor_info_t motor_info[MOTOR_NUM];         //定义电机数据结构体数组(8种ID)用于CAN1通信
mpu_angle_t yaw_info;   //定义yaw轴数据结构体

extern float yaw;
extern short gyroy,gyroz;
extern pid_t pid_mem[8];

//以下为各电机以及舵机的初值设定
float GM3508_target_speed = 2000; 

float PITCH_GM6020_target_speed = 1000;                         //速度单环调试所用
float PITCH_GM6020_target_speed2 = -1000;
float speedpitch = 0;
float PITCH_GM6020_target_angle_sys = 303;           //初始化时发射机构保持水平，角度范围是255°~312°
float PITCH_GM6020_target_angle;

float YAW_GM6020_target_speed = 2000;  //这个是对gyroz进行控制
float YAW_GM6020_target_angle = 30;

float GM2006_target_angle = 0;   


float Remote_magazine_value = 0;

/*
	CAN报文发送函数，对报文数据进行拼接、处理和打包（id_range取0或1对应3508电机两个不同的标识符）
	此处对3508电机、2006电机的电流进行设定
*/
void CAN1_Set_Current(uint16_t id_range,int16_t v1,int16_t v2,int16_t v3,int16_t v4)
{
	static CAN_TxHeaderTypeDef Tx_Header;   //发送句柄
	uint8_t tx_data[8];        //定义报文发送数组
	
	Tx_Header.StdId = (id_range==0)?(0x200):(0x1ff);   //ID标识符设置
  Tx_Header.IDE   = CAN_ID_STD;			             //标准格式
  Tx_Header.RTR   = CAN_RTR_DATA;                //数据帧
  Tx_Header.DLC   = 8;                         //8个字节数据
	
	//对发送的数据进行打包处理
  tx_data[0] = (v1>>8)&0xff;     //ID为1
  tx_data[1] =    (v1)&0xff;
	tx_data[2] = (v2>>8)&0xff;     //ID为2
  tx_data[3] =    (v2)&0xff;
	tx_data[4] = (v3>>8)&0xff;     //ID为3
  tx_data[5] =    (v3)&0xff;
	tx_data[6] = (v4>>8)&0xff;     //ID为4
  tx_data[7] =    (v4)&0xff;
  
	HAL_CAN_AddTxMessage(&hcan1, &Tx_Header, tx_data,(uint32_t*)CAN_TX_MAILBOX0);
}
/*
	CAN报文发送函数，对报文数据进行拼接、处理和打包（id_range取0或1对应6020电机两个不同的标识符）
	此处对GM6020的电压进行设定
*/
void CAN2_Set_Current(uint16_t id_range,int16_t v1,int16_t v2,int16_t v3,int16_t v4)
{
	static CAN_TxHeaderTypeDef Tx_Header;   //发送句柄
	uint8_t tx_data[8];        //定义报文发送数组
	
	Tx_Header.StdId = (id_range==0)?(0x1ff):(0x2ff);   //ID标识符设置
  Tx_Header.IDE   = CAN_ID_STD;			             //标准格式
  Tx_Header.RTR   = CAN_RTR_DATA;                //数据帧
  Tx_Header.DLC   = 8;                         //8个字节数据
	
	//对发送的数据进行打包处理
  tx_data[0] = (v1>>8)&0xff;     //ID为1
  tx_data[1] =    (v1)&0xff;
	tx_data[2] = (v2>>8)&0xff;     //ID为2
  tx_data[3] =    (v2)&0xff;
	tx_data[4] = (v3>>8)&0xff;     //ID为3
  tx_data[5] =    (v3)&0xff;
	tx_data[6] = (v4>>8)&0xff;     //ID为4
  tx_data[7] =    (v4)&0xff;
  
	HAL_CAN_AddTxMessage(&hcan2, &Tx_Header, tx_data, (uint32_t*)CAN_TX_MAILBOX1);
}

/*拨盘角度更新函数，用于2006电机控制的拨弹盘，每次调用计算并更新当前拨弹盘角度*/
void Update_angle(motor_info_t * motor)
{
	if(motor->rotor_angle - motor->last_angle>4096)           
		motor->round_cnt--;
	else if(motor->rotor_angle - motor->last_angle<-4096)
				 motor->round_cnt++;
	motor->last_angle  = motor->rotor_angle;                          //角度更新
	motor->total_angle = motor->round_cnt*8192+motor->rotor_angle;    //计算转过的总角度数值
	motor->dial_angle = motor->total_angle /8192.0f*360.0f;          //计算对应的拨盘角度
}

//获取yaw轴角度，用于输出yaw轴电机数据
void Get_yaw_angle(mpu_angle_t *p)
{
  static uint8_t init=0;
	p->now_angle = yaw;
	
	if(init)
	{
	  if(p->now_angle - p->last_angle>180)
			 p->round_cnt--;
		else if(p->now_angle - p->last_angle<-180)
		{
			 p->round_cnt++;
		}
	}
	else{
	  p->offset_angle=p->now_angle;                           //这句话用来初始化角度值
		init=1;                                                 
	}
	
	p->last_angle = p->now_angle;
	
	p->total_angle = p->round_cnt*360.0f+p->now_angle-p->offset_angle;      //根据YAW的值拓展总角度
	
}

void Motor_run_friction(void)
{
	Remote_friction();
	pid_calcu(&pid_mem[0],-GM3508_target_speed,motor_info[1].rotor_speed);   //3508(1)速度环
	pid_calcu(&pid_mem[1],GM3508_target_speed ,motor_info[2].rotor_speed);   //3508(2)速度环
}

void Motor_run_dial(void)
{
	Remote_dial();       
	Update_angle(&motor_info[0]);                                            //获取2006当前角度
	pid_calcu(&pid_mem[3],GM2006_target_angle ,motor_info[0].dial_angle );   //2006角度环（已经转为角度制，一发子弹对应36°即一转为10发子弹）
	pid_calcu(&pid_mem[2],pid_mem[3].out_value,motor_info[0].rotor_speed);   //2006速度环
}

void Motor_run_pitch(void)
{
	Remote_pitch();
	PITCH_GM6020_target_angle = PITCH_GM6020_target_angle_sys/360.0f*8191.0f;
	Limit(PITCH_GM6020_target_angle,5800,7100);                                  //输出限幅（待定哈）
	pid_calcu(&pid_mem[5],PITCH_GM6020_target_angle,motor_info[3].rotor_angle);  //pitch6020角度环
	pid_calcu(&pid_mem[4],-pid_mem[5].out_value,gyroy);  //pitch6020角度环
//	pid_calcu(&pid_mem[4],-pid_mem[5].out_value    ,gyroy+32                 );  //pitch6020速度环
	
	//速度单环调试代码(循环)
//		if(motor_info[3].rotor_angle>7700)
//		{		
//      speedpitch = PITCH_GM6020_target_speed;			
//		  pid_calcu(&pid_mem[4],speedpitch,gyroy+32);
//		}
//		else if(motor_info[3].rotor_angle<7000)
//		{	
//			speedpitch = PITCH_GM6020_target_speed2;
//			pid_calcu(&pid_mem[4],speedpitch,gyroy+32);
//		}
//		else{
//		  pid_calcu(&pid_mem[4],speedpitch,gyroy+32);
//		}		
	
		//速度单环调试代码(静止)
//		if(motor_info[3].rotor_angle>7700)
//		{		
//      //speedpitch = PITCH_GM6020_target_speed;			
//		  pid_calcu(&pid_mem[4],speedpitch,gyroy+32);
//		}
//		else if(motor_info[3].rotor_angle<7000)
//		{	
//			//speedpitch = PITCH_GM6020_target_speed2;
//			pid_calcu(&pid_mem[4],speedpitch,gyroy+32);
//		}
//		else{
//		  pid_calcu(&pid_mem[4],speedpitch,gyroy+32);
//		}		
}

void Motor_run_yaw(void)
{
	Remote_yaw();
	Get_yaw_angle(&yaw_info);                                                 //获取yaw轴信息
	pid_calcu(&pid_mem[7],YAW_GM6020_target_angle,yaw_info.total_angle);      //yaw6020角度环(角度环用度数作为输入）
	pid_calcu(&pid_mem[6],pid_mem[7].out_value,(int16_t)gyroz);               //yaw6020速度环（速度环应使用相应的角度值作为返回值
}
