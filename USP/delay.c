#include "delay.h"

void delay_us(uint32_t nus)
{
	//使用了tim4的内部时钟
  __HAL_TIM_SetCounter(&htim4,0);  
	__HAL_TIM_ENABLE(&htim4);
	while(__HAL_TIM_GetCounter(&htim4)<nus)
	{
	
	}
	__HAL_TIM_DISABLE(&htim4);
}
void delay_ms(uint16_t nms)
{
  HAL_Delay(nms);
}
