#ifndef __MOTOR_CTRL__
#define __MOTOR_CTRL__

#include "main.h"
#include "can.h"

#define MOTOR_NUM 5

/*定义电机数据结构类型*/
typedef struct
{
		//速度相关  
		uint16_t can_id;           //CAN的ID
    int16_t  set_current;			 //输出电流
    int16_t  torque_current;   //转矩电流
    int16_t  rotor_speed;			 //转子转速
    
    uint8_t  temp;             //电机温度
	
	  //角度相关
		uint16_t  rotor_angle; 		 //转子角度   0~8191   对应角度0~360°
		int32_t	  total_angle;
		uint16_t	offset_angle;
		uint16_t 	last_angle;
		
		int32_t   round_cnt;       //圆周数
		float     dial_angle;      //拨盘角度
		float     dial_angle_offset;
}motor_info_t;

//MPU数据结构体
typedef struct
{
  float now_angle;      //当前角度
	float last_angle;     //上次角度
	float total_angle;    //总角度
	float offset_angle;   //补偿角度
	int round_cnt;        //圆周期
}mpu_angle_t;

void CAN1_Set_Current(uint16_t,int16_t,int16_t,int16_t,int16_t);
void CAN2_Set_Current(uint16_t,int16_t,int16_t,int16_t,int16_t);
void Update_angle(motor_info_t *);
void Get_yaw_angle(mpu_angle_t *);
void Motor_run_friction(void);
void Motor_run_dial(void);
void Motor_run_pitch(void);
void Motor_run_yaw(void);





#endif
