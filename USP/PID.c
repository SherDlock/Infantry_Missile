#include "PID.h"

pid_t pid_mem[8];
/*
	PID运算成员：
	   pid_mem[0]   3508(1)速度环     |  pid_mem[1]   3508(2)速度环
		 pid_mem[2]   2006速度环        |  pid_mem[3]   2006角度环
		 pid_mem[4]   Pitch轴6020速度环 |  pid_mem[5]   Pitch轴6020角度环
		 pid_mem[6]   Yaw轴6020速度环   |  pid_mem[7]   Yaw轴6020角度环
*/

/**
* @brief  PID的初始化
* @param  *pid:PID参数结构
* @param  MaxOutput:总的输出限幅
* @param  IntegralLimit:积分输出限幅
* @return None.
*/
void pid_init(pid_t *pid,uint8_t pid_mode,float kp,float ki,float kd,uint32_t MaxOutput,uint32_t IntegralLimit)
{
	pid->mode = pid_mode;
  pid->kp=kp;
	pid->ki=ki;
  pid->kd=kd;
	pid->Max_Output = MaxOutput;
	pid->Integral_Limit = IntegralLimit;
}

/*PID计算函数*/
void pid_calcu(pid_t *pid,float target,float reality)
{
	//期望值和实际反馈值
	pid->target_value = target;    
	pid->feedback_value = reality;
	
	//if(pid->mode == 0)     //位置式
	//{
			
		pid->errors[LAST] = pid->errors[NOW]; //这次误差变为下次误差
		pid->errors[NOW] = pid->target_value - pid->feedback_value; //计算新的误差
		
		pid->p_out = pid->kp * pid->errors[NOW]; //比例
		
		pid->i_out += pid->ki * pid->errors[NOW]; //积分
	
		Limit(pid->i_out,-(pid->Integral_Limit),pid->Integral_Limit); //积分限幅
	
		if(pid->errors[NOW]-pid->errors[LAST] > 1500 || pid->errors[NOW]-pid->errors[LAST] <= -1500)
	  {
	    pid->d_out = 0;
	  }
		else
		{
	    pid->d_out = pid->kd * (pid->errors[NOW]-pid->errors[LAST]);  //微分
	  }
	
	/*	//积分分离
		if(pid->errors[NOW]-pid->errors[LAST] > 1500 || pid->errors[NOW]-pid->errors[LAST] <= -1500)
		{
			pid->i_out = 0;
		}
		else
		{
			pid->i_out += pid->ki * pid->errors[NOW]; //积分
			Limit(pid->i_out,-(pid->Integral_Limit),pid->Integral_Limit); //积分限幅
		}
		
		pid->d_out = pid->kd * (pid->errors[NOW]-pid->errors[LAST]);  //微分*/
		
//	}
//	else if(pid->mode == 1)     //增量式
//	{
//	pid->errors[LAST_LAST] = pid->errors[LAST];
//	pid->errors[LAST] = pid->errors[NOW];
//	pid->errors[NOW] = pid->target_value - pid->feedback_value;
//	
//	pid->p_out = pid->kp * (pid->errors[NOW]-pid->errors[LAST]); //比例
//	pid->i_out = pid->ki * pid->errors[NOW]; //积分(不是累加！)
//	pid->d_out = pid->kd * (pid->errors[NOW]-2*pid->errors[LAST]+pid->errors[LAST_LAST]);
//	}
	
	//计算PID
	pid->out_value = pid->p_out + pid->i_out + pid->d_out;
  Limit(pid->out_value, -(pid->Max_Output),pid->Max_Output); //输出限幅
}
