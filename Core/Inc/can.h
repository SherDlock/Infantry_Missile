/**
  ******************************************************************************
  * @file    can.h
  * @brief   This file contains all the function prototypes for
  *          the can.c file
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CAN_H__
#define __CAN_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "Motor_Ctrl.h"
/* USER CODE END Includes */

extern CAN_HandleTypeDef hcan1;
extern CAN_HandleTypeDef hcan2;

/* USER CODE BEGIN Private defines */
#define CAN_LINE_BUSY 1
#define CAN_SUCCESS 0
	
/* 定义CAN过滤器寄存器位宽类型 */
typedef union
{
	__IO uint32_t value; //32位的value变量，存储一个寄存器的值，而下面的结构体是对其进行细分
	struct
	{
		uint8_t REV : 1; //< [0] ：未使用，1bit
		uint8_t RTR : 1; //< [1] : RTR（数据帧或远程帧标志位），1bit
		uint8_t IDE : 1; //< [2] : IDE（标准帧或扩展帧标志位），1bit
		uint32_t EXID : 18; //< [21:3] : 存放扩展帧ID，18bits
		uint16_t STID : 11; //< [31:22]: 存放标准帧ID，11bits
	} Sub;
} CAN_FilterRegTypeDef;

/* USER CODE END Private defines */

void MX_CAN1_Init(void);
void MX_CAN2_Init(void);

/* USER CODE BEGIN Prototypes */
void CAN1_Filter_Config(uint32_t, uint16_t);
void CAN2_Filter_Config(uint32_t, uint16_t);

void myCAN_Start(CAN_HandleTypeDef *,CAN_HandleTypeDef *);
/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif

#endif /* __CAN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
