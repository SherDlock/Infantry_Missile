/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "can.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
//#include "stm32f4xx_it.h"
#include "dr16.h"
#include "UpperMonitor.h"
#include "stdio.h"
#include "mpu6050.h"
#include "inv_mpu.h"
#include "inv_mpu_dmp_motion_driver.h"
#include "PID.h"
#include "Motor_Ctrl.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
extern uint8_t sbus_rx_buffer[RC_FRAME_LENGTH]; //定长的数组用于接收DR16发来的数据
extern RC_Ctl_t RC_CtrlData;    //DR16遥控数据结构体
extern uint8_t Remote_flag; 

float pitch,roll,yaw; 		//欧拉角
short aacx,aacy,aacz;		  //加速度传感器原始数据
short gyrox,gyroy,gyroz;	//陀螺仪原始数据
short temp;				        //温度

/*-----------------临时定义-------------------------*/
float dial_ap = 10, dial_ai = 0.01, dial_ad = 0;
float dial_sp = 5, dial_si = 0.02, dial_sd = 0;
 
/*-----------------临时定义-------------------------*/


extern pid_t pid_mem[8];
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void MX_FREERTOS_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

//将MPU初始化
void MPU_All_Init(void)
{
	while(MPU_Init())						//初始化MPU6050
	{
	}
	
	while(mpu_dmp_init())//MPU DMP初始化
	{
	}
}


/*
	1.用于处理数据的任务
*/
//任务优先级
#define DATA_PROCESS_PRIO		6
//任务堆栈大小	
#define DATA_PROCESS_SIZE 	128 
//任务句柄
TaskHandle_t Data_Process_Handle;
//任务函数
void Data_Process(void *argument);

/*
	2.用于发送数据到上位机的函数
*/
//任务优先级
#define SEND_DATA_TO_UPPERMONITOR_PRIO		4
//任务堆栈大小	
#define SEND_DATA_TO_UPPERMONITOR_SIZE 		128  
//任务句柄
TaskHandle_t Send_Data_To_UpperMonitor_Handle;
//任务函数
void Send_Data_To_UpperMonitor(void *argument);

/*
	3.用于处理MPU的任务
*/
//任务优先级
#define MPU_PROCESS_PRIO		5
//任务堆栈大小	
#define MPU_PROCESS_SIZE 	256 
//任务句柄
TaskHandle_t MPU_Process_Handle;
//任务函数
void MPU_Process(void *argument);

/*
	4.用于GM3508电机、GM2006电机、GM6020电机控制的任务
*/
//任务优先级
#define	MOTOR_DRIVER_PRIO		5
//任务堆栈大小
#define MOTOR_DRIVER_SIZE		256
//任务句柄
TaskHandle_t Motor_Driver_Handle;
//任务函数
void Motor_Driver(void *argument);

/*
	5.队列
*/
#define MESSAGE_Q_NUM   4   	//发送数据的消息队列的数量 
QueueHandle_t Message_Queue;	//信息队列句柄

/*
	6.用于YAWGM3508电机的任务
*/
//任务优先级
#define	YAW_MOTOR_DRIVER_PRIO		5
//任务堆栈大小
#define YAW_MOTOR_DRIVER_SIZE		256
//任务句柄
TaskHandle_t YAW_Motor_Driver_Handle;
//任务函数
void YAW_Motor_Driver(void *argument);

//定义处理数据任务函数
void Data_Process(void *argument)
{

	   //不采用定时执行任务，而是每次收到信息都执行一次任务。
	uint8_t sbus_rx_buffer2[RC_FRAME_LENGTH]; //用于接收队列中的数据
	for(;;)
	{
		//从队列中接收到数据并且储存在sbus_rx_buffer2中
		if(xQueueReceive(Message_Queue,&sbus_rx_buffer2,portMAX_DELAY) == pdPASS)
		{
			//对接收到的数据进行解包
			RemoteDataProcess(sbus_rx_buffer2);
		}
	}
	
}

//定义发送数据任务函数
void Send_Data_To_UpperMonitor(void *argument)
{
  TickType_t xLastWakeTime_t = xTaskGetTickCount();
	TickType_t _xTicksToWait = pdMS_TO_TICKS(2);
	
	for(;;)
	{
	  Sent_Contorl(&huart1);
		vTaskDelayUntil(&xLastWakeTime_t, _xTicksToWait);
	}
	
}

//定义处理mpu数据任务函数
void MPU_Process(void *argument)
{
	TickType_t xLastWakeTime_t = xTaskGetTickCount();
  TickType_t _xTicksToWait = pdMS_TO_TICKS(2);
  for(;;)
	{
		//printf("准备获取数据");
	  if(mpu_dmp_get_data(&pitch,&roll,&yaw)==0)
		{
		  temp = MPU_Get_Temperature();	//得到温度值
			MPU_Get_Accelerometer(&aacx,&aacy,&aacz);	//得到加速度传感器数据
			MPU_Get_Gyroscope(&gyrox,&gyroy,&gyroz);	//得到陀螺仪数据
		}
	  vTaskDelayUntil(&xLastWakeTime_t, _xTicksToWait);
	}
}

//定义驱动电机的任务函数
void Motor_Driver(void *argument)
{
	TickType_t xLastWakeTime_t = xTaskGetTickCount();
  TickType_t _xTicksToWait = pdMS_TO_TICKS(5);
	
	/*电机PID控制初始化*/
	//摩擦轮电机1、2
	pid_init(&pid_mem[0],Position ,12 ,0.02 ,0,6000 ,7000);     //速度环
	pid_init(&pid_mem[1],Position ,12 ,0.02 ,0,6000 ,7000);
	
	//拨弹盘电机
	pid_init(&pid_mem[2],Position ,dial_ap ,dial_ai ,dial_ad,2000 ,4000);     //角度环  输出限幅原为6000
	pid_init(&pid_mem[3],Position ,dial_sp ,dial_si ,dial_sd,2000 ,4000);     //速度环  输出限幅原为5000
	
	//云台Pitch轴电机
	pid_init(&pid_mem[4],Position,12 ,0.03 ,0 ,30000,6000);
  pid_init(&pid_mem[5],Position,10 ,0    ,0 ,15000,5000);	
	
	//云台Yaw轴电机
	pid_init(&pid_mem[6],Position,30 ,0.22,0,30000,5000);	
	pid_init(&pid_mem[7],Position,220,0   ,0,12000,5000);
	
	for(;;)
	{
		if(Remote_flag<200)
		{
			//摩擦轮3508电机
			Motor_run_friction();
		
			//拨弹盘2006电机
			Motor_run_dial();
		
			//Pitch轴6020电机
			Motor_run_pitch();
			
//			//Yaw轴6020电机
//			Motor_run_yaw();
			
			//驱动弹仓舵机
//			Remote_Magazine_Servo();
			      
			//电机使能
			CAN1_Set_Current(0,pid_mem[2].out_value , pid_mem[0].out_value,pid_mem[1].out_value,0);
			CAN1_Set_Current(1,-pid_mem[4].out_value ,0                    ,0                   ,0);
//			CAN2_Set_Current(1,pid_mem[6].out_value ,0                    ,0                   ,0);

			//遥控标志增加，解包函数中置0
			Remote_flag++;                    
		}
		else                               
		{
			//遥控保护
			CAN1_Set_Current(0,0,0,0,0);
			CAN1_Set_Current(1,0,0,0,0);
			CAN2_Set_Current(1,0,0,0,0);
		}	
		vTaskDelayUntil(&xLastWakeTime_t, _xTicksToWait);
	}
}

//定义驱动电机的任务函数
void YAW_Motor_Driver(void *argument)
{
	TickType_t xLastWakeTime_t = xTaskGetTickCount();
  TickType_t _xTicksToWait = pdMS_TO_TICKS(5);
	for(;;)
	{
		if(Remote_flag<200)
		{
			//Yaw轴6020电机
			Motor_run_yaw();
			//电机使能
			CAN2_Set_Current(1,pid_mem[6].out_value ,0                    ,0                   ,0);
		}
		else                               
		{
			//遥控保护
			
			CAN2_Set_Current(1,0,0,0,0);
		}	
		vTaskDelayUntil(&xLastWakeTime_t, _xTicksToWait);
	}
}
	
//创建所有任务和队列的函数
void Task_Init()
{
  
	//创建处理数据任务
	xTaskCreate((TaskFunction_t )Data_Process,             
              (const char*    )"Data_Proccess",           
              (uint16_t       )DATA_PROCESS_SIZE,        
              (void*          )NULL,                  
              (UBaseType_t    )DATA_PROCESS_PRIO,        
              (TaskHandle_t*  )&Data_Process_Handle); 
								
	//创建发送数据任务
	xTaskCreate((TaskFunction_t )Send_Data_To_UpperMonitor,             
              (const char*    )"Send_Data_To_UpperMonitor",           
              (uint16_t       )SEND_DATA_TO_UPPERMONITOR_SIZE,        
              (void*          )NULL,                  
              (UBaseType_t    )SEND_DATA_TO_UPPERMONITOR_PRIO,        
              (TaskHandle_t*  )&Send_Data_To_UpperMonitor_Handle);		
	
	//创建MPU数据处理任务
  xTaskCreate((TaskFunction_t )MPU_Process,             
              (const char*    )"MPU_Proccess",           
              (uint16_t       )MPU_PROCESS_SIZE,        
              (void*          )NULL,                  
              (UBaseType_t    )MPU_PROCESS_PRIO,        
              (TaskHandle_t*  )&MPU_Process_Handle); 
								
	//创建电机驱动任务
	xTaskCreate((TaskFunction_t )Motor_Driver,
							(const char*    )"Motor_Run",
							(uint16_t       )MOTOR_DRIVER_SIZE,
							(void*          )NULL,
							(UBaseType_t    )MOTOR_DRIVER_PRIO,
							(TaskHandle_t*  )&Motor_Driver_Handle);
							
	//创建YAW电机驱动任务
	xTaskCreate((TaskFunction_t )YAW_Motor_Driver,
							(const char*    )"YAW_Motor_Run",
							(uint16_t       )YAW_MOTOR_DRIVER_SIZE,
							(void*          )NULL,
							(UBaseType_t    )YAW_MOTOR_DRIVER_PRIO,
							(TaskHandle_t*  )&YAW_Motor_Driver_Handle);
							
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART2_UART_Init();
  MX_TIM4_Init();
  MX_TIM5_Init();
  MX_CAN1_Init();
  MX_CAN2_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */

	//创建消息队列
	Message_Queue = xQueueCreate(MESSAGE_Q_NUM,RC_FRAME_LENGTH); //创建消息Message_Queue,队列项长度是串口接收缓冲区长度

	//设置过滤器，开启CAN接收中断等
	CAN1_Filter_Config(0x0000,0);
	CAN2_Filter_Config(0x0000,14);
	myCAN_Start(&hcan1,&hcan2);
	
	//开启DMA接收
	HAL_UART_Receive_DMA(&huart2,sbus_rx_buffer,RC_FRAME_LENGTH);
	
	//开启空闲接收中断
	__HAL_UART_ENABLE_IT(&huart1 ,UART_IT_IDLE);
	__HAL_UART_CLEAR_IDLEFLAG(&huart1); //清除标志位
	__HAL_UART_ENABLE_IT(&huart2 ,UART_IT_IDLE);
	__HAL_UART_CLEAR_IDLEFLAG(&huart2); //清除标志位
	
	//开启定时器
	HAL_TIM_Base_Start_IT(&htim5);
  HAL_TIM_PWM_Start(&htim5,TIM_CHANNEL_2);
	
	MPU_All_Init();    //MPU初始化
	
  Task_Init();       //创建任务
	
  /* USER CODE END 2 */

  /* Call init function for freertos objects (in freertos.c) */
  MX_FREERTOS_Init();
  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

 /**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM7 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM7) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
