/**
  ******************************************************************************
  * @file    can.c
  * @brief   This file provides code for the configuration
  *          of the CAN instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "can.h"

/* USER CODE BEGIN 0 */
extern motor_info_t motor_info[8];         //定义电机数据结构体数组(8种ID)用于CAN1通信

/* USER CODE END 0 */

CAN_HandleTypeDef hcan1;
CAN_HandleTypeDef hcan2;

/* CAN1 init function */
void MX_CAN1_Init(void)
{

  hcan1.Instance = CAN1;
  hcan1.Init.Prescaler = 3;
  hcan1.Init.Mode = CAN_MODE_NORMAL;
  hcan1.Init.SyncJumpWidth = CAN_SJW_1TQ;
  hcan1.Init.TimeSeg1 = CAN_BS1_9TQ;
  hcan1.Init.TimeSeg2 = CAN_BS2_4TQ;
  hcan1.Init.TimeTriggeredMode = DISABLE;
  hcan1.Init.AutoBusOff = ENABLE;
  hcan1.Init.AutoWakeUp = ENABLE;
  hcan1.Init.AutoRetransmission = ENABLE;
  hcan1.Init.ReceiveFifoLocked = DISABLE;
  hcan1.Init.TransmitFifoPriority = DISABLE;
  if (HAL_CAN_Init(&hcan1) != HAL_OK)
  {
    Error_Handler();
  }

}
/* CAN2 init function */
void MX_CAN2_Init(void)
{

  hcan2.Instance = CAN2;
  hcan2.Init.Prescaler = 3;
  hcan2.Init.Mode = CAN_MODE_NORMAL;
  hcan2.Init.SyncJumpWidth = CAN_SJW_1TQ;
  hcan2.Init.TimeSeg1 = CAN_BS1_9TQ;
  hcan2.Init.TimeSeg2 = CAN_BS2_4TQ;
  hcan2.Init.TimeTriggeredMode = DISABLE;
  hcan2.Init.AutoBusOff = ENABLE;
  hcan2.Init.AutoWakeUp = ENABLE;
  hcan2.Init.AutoRetransmission = ENABLE;
  hcan2.Init.ReceiveFifoLocked = DISABLE;
  hcan2.Init.TransmitFifoPriority = DISABLE;
  if (HAL_CAN_Init(&hcan2) != HAL_OK)
  {
    Error_Handler();
  }

}

static uint32_t HAL_RCC_CAN1_CLK_ENABLED=0;

void HAL_CAN_MspInit(CAN_HandleTypeDef* canHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(canHandle->Instance==CAN1)
  {
  /* USER CODE BEGIN CAN1_MspInit 0 */

  /* USER CODE END CAN1_MspInit 0 */
    /* CAN1 clock enable */
    HAL_RCC_CAN1_CLK_ENABLED++;
    if(HAL_RCC_CAN1_CLK_ENABLED==1){
      __HAL_RCC_CAN1_CLK_ENABLE();
    }

    __HAL_RCC_GPIOA_CLK_ENABLE();
    /**CAN1 GPIO Configuration
    PA11     ------> CAN1_RX
    PA12     ------> CAN1_TX
    */
    GPIO_InitStruct.Pin = GPIO_PIN_11|GPIO_PIN_12;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF9_CAN1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* CAN1 interrupt Init */
    HAL_NVIC_SetPriority(CAN1_RX0_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(CAN1_RX0_IRQn);
    HAL_NVIC_SetPriority(CAN1_RX1_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(CAN1_RX1_IRQn);
  /* USER CODE BEGIN CAN1_MspInit 1 */

  /* USER CODE END CAN1_MspInit 1 */
  }
  else if(canHandle->Instance==CAN2)
  {
  /* USER CODE BEGIN CAN2_MspInit 0 */

  /* USER CODE END CAN2_MspInit 0 */
    /* CAN2 clock enable */
    __HAL_RCC_CAN2_CLK_ENABLE();
    HAL_RCC_CAN1_CLK_ENABLED++;
    if(HAL_RCC_CAN1_CLK_ENABLED==1){
      __HAL_RCC_CAN1_CLK_ENABLE();
    }

    __HAL_RCC_GPIOB_CLK_ENABLE();
    /**CAN2 GPIO Configuration
    PB12     ------> CAN2_RX
    PB13     ------> CAN2_TX
    */
    GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_13;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF9_CAN2;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /* CAN2 interrupt Init */
    HAL_NVIC_SetPriority(CAN2_RX1_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(CAN2_RX1_IRQn);
  /* USER CODE BEGIN CAN2_MspInit 1 */

  /* USER CODE END CAN2_MspInit 1 */
  }
}

void HAL_CAN_MspDeInit(CAN_HandleTypeDef* canHandle)
{

  if(canHandle->Instance==CAN1)
  {
  /* USER CODE BEGIN CAN1_MspDeInit 0 */

  /* USER CODE END CAN1_MspDeInit 0 */
    /* Peripheral clock disable */
    HAL_RCC_CAN1_CLK_ENABLED--;
    if(HAL_RCC_CAN1_CLK_ENABLED==0){
      __HAL_RCC_CAN1_CLK_DISABLE();
    }

    /**CAN1 GPIO Configuration
    PA11     ------> CAN1_RX
    PA12     ------> CAN1_TX
    */
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_11|GPIO_PIN_12);

    /* CAN1 interrupt Deinit */
    HAL_NVIC_DisableIRQ(CAN1_RX0_IRQn);
    HAL_NVIC_DisableIRQ(CAN1_RX1_IRQn);
  /* USER CODE BEGIN CAN1_MspDeInit 1 */

  /* USER CODE END CAN1_MspDeInit 1 */
  }
  else if(canHandle->Instance==CAN2)
  {
  /* USER CODE BEGIN CAN2_MspDeInit 0 */

  /* USER CODE END CAN2_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_CAN2_CLK_DISABLE();
    HAL_RCC_CAN1_CLK_ENABLED--;
    if(HAL_RCC_CAN1_CLK_ENABLED==0){
      __HAL_RCC_CAN1_CLK_DISABLE();
    }

    /**CAN2 GPIO Configuration
    PB12     ------> CAN2_RX
    PB13     ------> CAN2_TX
    */
    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_12|GPIO_PIN_13);

    /* CAN2 interrupt Deinit */
    HAL_NVIC_DisableIRQ(CAN2_RX1_IRQn);
  /* USER CODE BEGIN CAN2_MspDeInit 1 */

  /* USER CODE END CAN2_MspDeInit 1 */
  }
}

/* USER CODE BEGIN 1 */

/**
* @brief CAN过滤器配置函数
* @parma _id：目标ID号，标准ID
* @parma _filter_num：当前配置的过滤器组号，0-27
* @return NULL
* @note _id配置为全0时，表示全通
*/
void CAN1_Filter_Config(uint32_t _id, uint16_t _filter_num)
{
	CAN_FilterTypeDef sFilterConfig;
	/* 过滤器配置，参照HAL库CAN_FilterTypeDef的相关注释完成
	配置模式、位宽、ID、MaskID、关联FIFO、使能激活 */
	
	//设置为全通模式
	sFilterConfig.FilterIdHigh=           0X0000;
	sFilterConfig.FilterIdLow=            0x0000;
	sFilterConfig.FilterMaskIdHigh=       0X0000;
	sFilterConfig.FilterMaskIdLow=        0x0000;
	
	sFilterConfig.FilterBank=             _filter_num;            //过滤器编号
	sFilterConfig.FilterFIFOAssignment=   CAN_RX_FIFO0;           //存入FIFO0
	sFilterConfig.FilterActivation=       ENABLE;                 //使能过滤器
	sFilterConfig.FilterMode=             CAN_FILTERMODE_IDMASK;  //掩码模式
	sFilterConfig.FilterScale=            CAN_FILTERSCALE_32BIT;  //32位宽
	sFilterConfig.SlaveStartFilterBank=   14;

	if (HAL_CAN_ConfigFilter(&hcan1, &sFilterConfig) != HAL_OK)   //加载过滤器配置
	{
		Error_Handler();
	}
}

void CAN2_Filter_Config(uint32_t _id, uint16_t _filter_num)
{
	CAN_FilterTypeDef sFilterConfig;
	/* 过滤器配置，参照HAL库CAN_FilterTypeDef的相关注释完成
	配置模式、位宽、ID、MaskID、关联FIFO、使能激活 */
	
	//设置为全通模式
	sFilterConfig.FilterIdHigh=           0X0000;
	sFilterConfig.FilterIdLow=            0x0000;
	sFilterConfig.FilterMaskIdHigh=       0X0000;
	sFilterConfig.FilterMaskIdLow=        0x0000;
	
	sFilterConfig.FilterBank=             _filter_num;            //过滤器编号
	sFilterConfig.FilterFIFOAssignment=   CAN_RX_FIFO1;           //存入FIFO1
	sFilterConfig.FilterActivation=       ENABLE;                 //使能过滤器
	sFilterConfig.FilterMode=             CAN_FILTERMODE_IDMASK;  //掩码模式
	sFilterConfig.FilterScale=            CAN_FILTERSCALE_32BIT;  //32位宽
	sFilterConfig.SlaveStartFilterBank=   14;

	if (HAL_CAN_ConfigFilter(&hcan2, &sFilterConfig) != HAL_OK)   //加载过滤器配置
	{
		Error_Handler();
	}
}

//FIFO接收中断回调函数，用于接收电机的数据，不需要声明，FIFO接收数据之后立即调用
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
	CAN_RxHeaderTypeDef Rx_Header;  //接受句柄
	uint8_t data_CAN1[8];
	
	//判断CAN数据接收
	if (hcan->Instance == CAN1)
	{
		//获得接收到的数据头和数据
		if (HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO0, &Rx_Header, data_CAN1) == HAL_OK)
		{
			//判断接收到的报文ID号，并作出对应的解包处理(0X201~0X204)
			if(Rx_Header.StdId > 0x200 && Rx_Header.StdId <= 0x204)   
			{
				/* 解包处理 */
				uint16_t motor_id = Rx_Header.StdId - 0X200;   //计算出电机的ID
				motor_info[motor_id-1].rotor_angle    = ((data_CAN1[0] << 8) | data_CAN1[1]);    //内存对齐
				motor_info[motor_id-1].rotor_speed    = ((data_CAN1[2] << 8) | data_CAN1[3]);
				motor_info[motor_id-1].torque_current = ((data_CAN1[4] << 8) | data_CAN1[5]);
				motor_info[motor_id-1].temp           =   data_CAN1[6];
				motor_info[motor_id-1].can_id         = Rx_Header.StdId;
			}
			else if(Rx_Header.StdId > 0x204 && Rx_Header.StdId <= 0x20c)
			{
				/* 解包处理 */
			  uint16_t motor_id = Rx_Header.StdId - 0X204;   //计算出电机的ID
				motor_info[motor_id+2].rotor_angle    = ((data_CAN1[0] << 8) | data_CAN1[1]);    //内存对齐
				motor_info[motor_id+2].rotor_speed    = ((data_CAN1[2] << 8) | data_CAN1[3]);
				motor_info[motor_id+2].torque_current = ((data_CAN1[4] << 8) | data_CAN1[5]);
				motor_info[motor_id+2].temp           =   data_CAN1[6];
				motor_info[motor_id+2].can_id         = Rx_Header.StdId;
			}
			HAL_CAN_ActivateNotification (&hcan1,CAN_IT_RX_FIFO0_MSG_PENDING);     //重新使能FIFO0接收中断
		}
	}
}

//FIFO接收中断回调函数，用于接收电机的数据，不需要声明，FIFO接收数据之后立即调用
void HAL_CAN_RxFifo1MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
	CAN_RxHeaderTypeDef Rx_Header;  //接受句柄
	uint8_t data_CAN2[8];
	
	//判断CAN数据接收
	if (hcan->Instance == CAN2)
	{
		//获得接收到的数据头和数据
		if (HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO1, &Rx_Header, data_CAN2) == HAL_OK)
		{
			//判断接收到的报文ID号，并作出对应的解包处理(0X204~0X20B)
			if(Rx_Header.StdId > 0x204 && Rx_Header.StdId <= 0x20C)   
			{
				/* 解包处理 */
				uint16_t motor_id = Rx_Header.StdId - 0X204;   //计算出电机的ID
				motor_info[motor_id+3].rotor_angle    = ((data_CAN2[0] << 8) | data_CAN2[1]);
				motor_info[motor_id+3].rotor_speed    = ((data_CAN2[2] << 8) | data_CAN2[3]);
				motor_info[motor_id+3].torque_current = ((data_CAN2[4] << 8) | data_CAN2[5]);
				motor_info[motor_id+3].temp           =   data_CAN2[6];
				motor_info[motor_id+3].can_id         = Rx_Header.StdId;
			}
			HAL_CAN_ActivateNotification (&hcan2,CAN_IT_RX_FIFO1_MSG_PENDING);    //重新使能FIFO1接收中断
		}
	}
}

/* CAN启动函数 */
void myCAN_Start(CAN_HandleTypeDef *hcan1,CAN_HandleTypeDef *hcan2)
{
	if (HAL_CAN_ActivateNotification(hcan1 ,CAN_IT_RX_FIFO0_MSG_PENDING)!= HAL_OK)   //使能CAN中断
	{
		Error_Handler();
	}
	if (HAL_CAN_ActivateNotification(hcan2 ,CAN_IT_RX_FIFO1_MSG_PENDING)!= HAL_OK)   //使能CAN中断
	{
		Error_Handler();
	}
	if (HAL_CAN_Start(hcan1)!= HAL_OK)   //使能CAN外设
	{
		Error_Handler();
	}
	if (HAL_CAN_Start(hcan2)!= HAL_OK)   //使能CAN外设
	{
		Error_Handler();
	}
}
/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
